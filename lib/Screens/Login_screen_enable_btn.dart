import 'package:flutter/material.dart';
import 'package:kss_login/Constants/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoginScreenEnableBtn extends StatefulWidget {

  @override
  _LoginScreenEnableBtnState createState() => _LoginScreenEnableBtnState();
}

class _LoginScreenEnableBtnState extends State<LoginScreenEnableBtn> {
  bool _btnEnabled = false;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors:[
                      kkBlueColor, kBlueLightColor
                    ]
                )
            )
        ),
        Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          body: Column(
            children: [
              Container(
                width:81,
                height: 32,
                margin: EdgeInsets.only(top: 68, right: 20, left: 280),
                //padding: const EdgeInsets.only(left: 4, right: 4, top: 4, bottom: 4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    border: Border.all(color: Colors.white.withOpacity(0.5)),
                    color: Colors.grey.withOpacity(0.4)

                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        //borderRadius: BorderRadius.circular(20),
                          image: DecorationImage(
                            image: AssetImage('assets/images/flag.png'),
                          )
                      ),
                    ),
                    Text('EN', style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: Colors.white
                    ),),
                    SvgPicture.asset('assets/icons/Vector.svg'),

                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 68),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('KS', style: TextStyle(
                        color: kOrangeColor,
                        fontSize: 36,
                        fontWeight: FontWeight.w700
                    ),),
                    Text('Securities', style: TextStyle(
                        color: Colors.white,
                        fontSize: 36,
                        fontWeight: FontWeight.w700
                    ),)
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Container(
                height: 56,
                width: 327,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white70.withOpacity(0.3),
                    border: Border.all(color: Colors.white)
                ),
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 16, right: 16,top: 8),
                    labelText: 'Tên Đăng Nhập',
                    labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                        height: 1.2
                    ),
                  ),
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                      height: 1.2
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                height: 56,
                width: 327,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white70.withOpacity(0.3),
                    border: Border.all(color: Colors.white)
                ),
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 16,right: 16,top: 8),
                    //hintText: 'Mật Khẩu',
                    labelText: 'Mật Khẩu',
                    labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                        height: 1.2
                    ),
                  ),
                  obscureText: true,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                      height:1.2
                  ),
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Container(
                margin: EdgeInsets.only(left: 156, right: 45),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Quên Mật Khẩu', style: TextStyle(
                        fontSize: 14,
                        color: Colors.white
                    ),),
                    Container(
                      height: 20,
                      width: 1.14,
                      color: Colors.white,
                    ),
                    Text('Đăng kí',style: TextStyle(
                        fontSize: 14,
                        color: Colors.white
                    ),
                    )
                  ],
                ),
              ),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      blurRadius: 1,
                      spreadRadius: 2,
                      offset: Offset(0,4),
                    )
                  ],
                ),

                margin: EdgeInsets.only(top: 20, bottom: 188),
                child: ButtonTheme(
                  minWidth: 327,
                  height: 44,
                  child: FlatButton(
                    onPressed:(){},
                    color: kOrangeColor,
                    child: Text("Đăng Nhập", style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.white
                    ),),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 68,right: 60),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.only(bottom: 6),
                          width: 48,
                          height: 48,
                          child:Container(
                            margin: EdgeInsets.all(8),
                            child: SvgPicture.asset('assets/icons/withdraw.svg'),
                          ),
                        ),
                        Text('Rút Tiền', style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Colors.white
                        ),),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 6,left: 4),
                          width: 48,
                          height: 48,
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          child:Container(
                            margin: EdgeInsets.all(8),
                            child: SvgPicture.asset('assets/icons/income.svg'),
                          ),
                        ),
                        Text('Nộp Tiền', style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Colors.white
                        ),),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.only(bottom: 6),
                          width: 48,
                          height: 48,
                          child:Container(
                            margin: EdgeInsets.all(8),
                            child: SvgPicture.asset('assets/icons/transfer.svg'),
                          ),
                        ),

                        Text('Chuyển Tiền', style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Colors.white
                        ),),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
