import 'package:flutter/material.dart';
import 'package:kss_login/Constants/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LoginWithShortcutFinger extends StatefulWidget {

  @override
  _LoginWithShortcutFingerState createState() => _LoginWithShortcutFingerState();
}


class _LoginWithShortcutFingerState extends State<LoginWithShortcutFinger> {
  bool _btnEnabled = false;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors:[
                      kkBlueColor, kBlueLightColor
                    ]
                )
            )
        ),
        Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          body: Column(
            children: [
              Container(
                width:81,
                height: 32,
                margin: EdgeInsets.only(top: 68, right: 20, left: 280),
                //padding: const EdgeInsets.only(left: 4, right: 4, top: 4, bottom: 4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    border: Border.all(color: Colors.white.withOpacity(0.5)),
                    color: Colors.grey.withOpacity(0.4)

                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        //borderRadius: BorderRadius.circular(20),
                          image: DecorationImage(
                            image: AssetImage('assets/images/flag.png'),
                          )
                      ),
                    ),
                    Text('EN', style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18,
                        color: Colors.white
                    ),),
                    SvgPicture.asset('assets/icons/Vector.svg'),

                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 68),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('KS', style: TextStyle(
                        color: kOrangeColor,
                        fontSize: 36,
                        fontWeight: FontWeight.w700
                    ),),
                    Text('Securities', style: TextStyle(
                        color: Colors.white,
                        fontSize: 36,
                        fontWeight: FontWeight.w700
                    ),)
                  ],
                ),
              ),
              SizedBox(
                height: 28,
              ),
              Container(
                  height: 56,
                  width: 327,
                  color: Colors.transparent,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('Xin chào', style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                          height: 0.8
                      ),),
                      SizedBox(
                        height: 6,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text('Nguyễn Văn A',style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                              color: Colors.white
                          ),),
                          Container(
                            margin: EdgeInsets.only(left: 8),
                            width: 13.33,
                            height: 13.33,
                            color: Colors.transparent,
                            child: Stack(
                              children: [
                                SvgPicture.asset('assets/icons/circle577.svg'),
                                Container(
                                  margin: EdgeInsets.only(left: 4.33, top: 2.33, bottom: 2.33, right: 3),
                                  width: 6,
                                  height: 8.67,
                                  color: Colors.transparent,
                                  child: SvgPicture.asset('assets/icons/Vector253.svg'),
                                ),

                              ],
                            ),
                          )
                        ],
                      )
                    ],
                  )
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                height: 56,
                width: 327,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white70.withOpacity(0.3),
                    border: Border.all(color: Colors.white)
                ),
                child: TextField(
                  decoration: InputDecoration(
                    suffixIcon: Icon(
                      Icons.visibility,
                      color: Colors.white.withOpacity(0.6),
                    ),

                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 16,right: 16,top: 8),
                    labelText: 'Mật Khẩu',
                    labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Colors.white,
                        height: 1.2
                    ),
                  ),
                  obscureText: true,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: Colors.white,
                      height: 1.2
                  ),
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Container(
                margin: EdgeInsets.only( right: 45),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text('Quên Mật Khẩu', style: TextStyle(
                        fontSize: 14,
                        color: Colors.white
                    ),
                    ),]
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.1),
                      blurRadius: 1,
                      spreadRadius: 2,
                      offset: Offset(0,4),
                    )
                  ],
                ),

                child: ButtonTheme(
                  minWidth: 327,
                  height: 44,
                  child: FlatButton(
                    onPressed:(){},
                    color: kOrangeColor,
                    child: Text("Đăng Nhập", style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.white
                    ),),
                  ),
                ),
              ),
              SizedBox(
                height: 22,
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.only(bottom: 136),
                  width: 44,
                  height: 44,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.2),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Container(
                      margin: EdgeInsets.all(6),
                      child: SvgPicture.asset('assets/icons/fingerprint.svg',color: Colors.white,),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 68,right: 60),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.only(bottom: 6),
                          width: 48,
                          height: 48,
                          child:Container(
                            margin: EdgeInsets.all(8),
                            child: SvgPicture.asset('assets/icons/withdraw.svg'),
                          ),
                        ),
                        Text('Rút Tiền', style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Colors.white
                        ),),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 6,left: 4),
                          width: 48,
                          height: 48,
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          child:Container(
                            margin: EdgeInsets.all(8),
                            child: SvgPicture.asset('assets/icons/income.svg'),
                          ),
                        ),
                        Text('Nộp Tiền', style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Colors.white
                        ),),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          margin: EdgeInsets.only(bottom: 6),
                          width: 48,
                          height: 48,
                          child:Container(
                            margin: EdgeInsets.all(8),
                            child: SvgPicture.asset('assets/icons/transfer.svg'),
                          ),
                        ),

                        Text('Chuyển Tiền', style: TextStyle(
                            fontWeight: FontWeight.w400,
                            fontSize: 12,
                            color: Colors.white
                        ),),
                      ],
                    )
                  ],
                ),
              )

            ],
          ),
        )
      ],
    );
  }
}
