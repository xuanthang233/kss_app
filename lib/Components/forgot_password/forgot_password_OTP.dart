
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kss_login/Constants/constants.dart';

class ForgotPasswordOtp extends StatefulWidget {

  @override
  _ForgotPasswordOtpState createState() => _ForgotPasswordOtpState();
}

class _ForgotPasswordOtpState extends State<ForgotPasswordOtp> {

  int _value= 1;

  FocusNode pin2Node= new FocusNode();
  FocusNode pin3Node= new FocusNode();
  FocusNode pin4Node= new FocusNode();
  FocusNode pin5Node= new FocusNode();
  FocusNode pin6Node= new FocusNode();

  @override
  void initState(){
    super.initState();
    pin2Node= FocusNode();
    pin3Node= FocusNode();
    pin4Node= FocusNode();
    pin5Node= FocusNode();
    pin6Node= FocusNode();

  }
  @override
  void dispose(){
    pin2Node.dispose();
    pin3Node.dispose();
    pin4Node.dispose();
    pin5Node.dispose();
    pin6Node.dispose();
    super.dispose();
  }
  void nextfield(String value, FocusNode focusnode){
    if(value.length==1){
      focusnode.requestFocus();
    }
  }
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(top:20),
                height: 40,
                decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1, color:KBorderColor.withOpacity(0.2)),
                    ),
                    color: Colors.white
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height:18,
                      width: 11,
                      margin: EdgeInsets.only(left: 23, top: 6),
                      child: SvgPicture.asset('assets/icons/Vector_back.svg'),
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 102, top: 6),
                        child: Text('Cấp lại mật khẩu', style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: KBorderColor
                        ),)
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 33,
              ),
              Container(
                height: 30,
                margin: EdgeInsets.only(left: 10, right: 10),
                color: Colors.white,
                child: Text('Xác nhận OTP', style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                  color: KBorderColor,
                  height: 1.3,
                ),),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                height: 60,
                width: 343,
                color: Colors.white,
                child: RichText(
                  text: TextSpan(
                    text: 'Mã OTP đã được gửi về số điện thoại ',
                    style: TextStyle(
                      color: KBorderColor.withOpacity(0.6),
                      fontSize: 14,
                      fontWeight: FontWeight.normal,
                      height: 1.2
                    ),
                    children: [
                      TextSpan(
                        text: '<SĐT khách hàng> ',
                        style: TextStyle(
                            color: KBorderColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          height: 1.2
                        ),
                      ),
                      TextSpan(
                        text: 'của quý khách. Vui lòng nhập và xác nhận mã OTP để tiếp tục.',
                        style: TextStyle(
                            color: KBorderColor.withOpacity(0.6),
                            fontSize: 14,
                            fontWeight: FontWeight.normal,
                            height: 1.2
                        ),
                      ),
                    ]
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                margin: EdgeInsets.only(left: 24, right: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      height: 48,
                      width: 48,
                      decoration: BoxDecoration(
                        border: Border.all(width: 1,color: KBorderColor.withOpacity(0.2)),
                        borderRadius: BorderRadius.circular(4)
                      ),
                      child: TextFormField(
                        autofocus: true,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: KBorderColor,
                          height: 1.2
                        ),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: '-',
                          hintStyle: TextStyle(
                            fontSize: 20,
                            color: KBorderColor.withOpacity(0.2)
                          )
                        ),
                        onChanged: (value){
                          nextfield(value, pin2Node);
                        },
                      ),
                    ),
                    Container(
                      height: 48,
                      width: 48,
                      decoration: BoxDecoration(
                          border: Border.all(width: 1,color: KBorderColor.withOpacity(0.2)),
                          borderRadius: BorderRadius.circular(4)
                      ),
                      child: TextFormField(
                        focusNode: pin2Node,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: KBorderColor,
                            height: 1.2
                        ),
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: '-',
                            hintStyle: TextStyle(
                                fontSize: 20,
                                color: KBorderColor.withOpacity(0.2)
                            )
                        ),
                        onChanged: (value){
                          nextfield(value, pin3Node);
                        },
                      ),
                    ),
                    Container(
                      height: 48,
                      width: 48,
                      decoration: BoxDecoration(
                          border: Border.all(width: 1,color: KBorderColor.withOpacity(0.2)),
                          borderRadius: BorderRadius.circular(4)
                      ),
                      child: TextFormField(
                        focusNode: pin3Node,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: KBorderColor,
                            height: 1.2
                        ),
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: '-',
                            hintStyle: TextStyle(
                                fontSize: 20,
                                color: KBorderColor.withOpacity(0.2)
                            )
                        ),
                        onChanged: (value){
                          nextfield(value, pin4Node);
                        },
                      ),
                    ),
                    Container(
                      height: 48,
                      width: 48,
                      decoration: BoxDecoration(
                          border: Border.all(width: 1,color: KBorderColor.withOpacity(0.2)),
                          borderRadius: BorderRadius.circular(4)
                      ),
                      child: TextFormField(
                        focusNode: pin4Node,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: KBorderColor,
                            height: 1.2
                        ),
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: '-',
                            hintStyle: TextStyle(
                                fontSize: 20,
                                color: KBorderColor.withOpacity(0.2)
                            )
                        ),
                        onChanged: (value){
                          nextfield(value, pin5Node);
                        },
                      ),
                    ),
                    Container(
                      height: 48,
                      width: 48,
                      decoration: BoxDecoration(
                          border: Border.all(width: 1,color: KBorderColor.withOpacity(0.2)),
                          borderRadius: BorderRadius.circular(4)
                      ),
                      child: TextFormField(
                        focusNode: pin5Node,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: KBorderColor,
                            height: 1.2
                        ),
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: '-',
                            hintStyle: TextStyle(
                                fontSize: 20,
                                color: KBorderColor.withOpacity(0.2)
                            )
                        ),
                        onChanged: (value){
                          nextfield(value, pin6Node);
                        },
                      ),
                    ),
                    Container(
                      height: 48,
                      width: 48,
                      decoration: BoxDecoration(
                          border: Border.all(width: 1,color: KBorderColor.withOpacity(0.2)),
                          borderRadius: BorderRadius.circular(4)
                      ),
                      child: TextFormField(
                        focusNode: pin6Node,
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: KBorderColor,
                            height: 1.2
                        ),
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: '-',
                            hintStyle: TextStyle(
                                fontSize: 20,
                                color: KBorderColor.withOpacity(0.2)
                            )
                        ),
                        onChanged: (value){
                          pin6Node.unfocus();
                        },
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 20,
                width: 343,
                color: Colors.white,
                child: RichText(
                  text: TextSpan(
                    text: 'Qúy khách chưa nhận được mã ? ',
                    style: TextStyle(
                        color: KBorderColor.withOpacity(0.6),
                        fontSize: 14,
                        fontWeight: FontWeight.normal,
                        height: 1.2
                    ),
                    children: [
                      TextSpan(
                        text: 'Gửi lại OTP (60s)',
                        style: TextStyle(
                            color: kkBlueColor,
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            height: 1.2
                        )
                      )
                    ]
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );

  }
}
