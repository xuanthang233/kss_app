
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kss_login/Constants/constants.dart';

class ForgotPasswordPhoneNumber extends StatefulWidget {

  @override
  _ForgotPasswordPhoneNumberState createState() => _ForgotPasswordPhoneNumberState();
}

class _ForgotPasswordPhoneNumberState extends State<ForgotPasswordPhoneNumber> {
  int _value= 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(top:20),
                height: 40,
                decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(width: 1, color:KBorderColor.withOpacity(0.2)),
                    ),
                    color: Colors.white
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height:18,
                      width: 11,
                      margin: EdgeInsets.only(left: 23, top: 6),
                      child: SvgPicture.asset('assets/icons/Vector_back.svg'),
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 108, top: 6),
                        child: Text('Cấp lại mật khẩu', style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: KBorderColor
                        ),)
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                height: 20,
                color: Colors.white,
                margin: EdgeInsets.only(left: 16, right: 104),
                child: Text(
                  'Số điện thoại bạn dùng để đăng kí tài khoản',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.normal,
                    color: KBorderColor.withOpacity(0.6),
                    height: 1.2
                  ),
                ),
              ),

              SizedBox(
                height: 16,
              ),
              Container(
                height: 56,
                margin: EdgeInsets.only(left: 16, right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white,
                    border: Border.all(
                      width: 1, color: KBorderColor.withOpacity(0.1),
                    )
                ),
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 16,right: 16,top: 8),
                    labelText: 'Số điện thoại',
                    labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: KBorderColor.withOpacity(0.4),
                        height: 1.2
                    ),
                  ),
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: KBorderColor,
                      height: 1.2
                  ),
                ),
              ),
              SizedBox(
                height: 531,
              ),
              Container(
                height: 60,
                decoration: BoxDecoration(
                    border: Border(
                        top: BorderSide(width: 1, color:KBorderColor.withOpacity(0.2))
                    ),
                    color: Colors.white
                ),
                child: Container(
                  margin: EdgeInsets.only(left: 16, right: 16,top: 8, bottom: 8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.1),
                        blurRadius: 1,
                        spreadRadius: 2,
                        offset: Offset(0,4),
                      )
                    ],
                  ),
                  child: ButtonTheme(
                    minWidth: 362,
                    height: 44,
                    child: FlatButton(
                      onPressed:(){},
                      color: kkBlueColor,
                      child: Text("Tiếp tục", style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                          color: Colors.white
                      ),),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );

  }
}
