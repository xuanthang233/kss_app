
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kss_login/Constants/constants.dart';

class FogotPassword extends StatefulWidget {



  @override
  _FogotPasswordState createState() => _FogotPasswordState();
}

class _FogotPasswordState extends State<FogotPassword> {
  bool _isChecked=false;
  int _value= 1;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.only(top:20),
                height: 40,
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(width: 1, color:KBorderColor.withOpacity(0.2)),
                  ),
                  color: Colors.white
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      height:18,
                      width: 11,
                      margin: EdgeInsets.only(left: 23, top: 6),
                      child: SvgPicture.asset('assets/icons/Vector_back.svg'),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 108, top: 6),
                        child: Text('Cấp lại mật khẩu', style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: KBorderColor
                        ),)
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                height: 40,
                color: Colors.white,
                margin: EdgeInsets.only(left: 16, right: 16),
                child: Text(
                  'Bạn cần điền đầy đủ thông tin cá nhân để tiến hành cấp lại mẩu khẩu mới',
                  style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 14,
                    color: KBorderColor.withOpacity(0.6),
                    height: 1.2
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 8, right: 24),
                    child: Row(
                      children: [
                        Radio(
                          value: 1,
                          groupValue: _value,
                          activeColor: Colors.blue,
                          onChanged: (value){
                            setState(() {
                              _value = value.hashCode;
                            });
                          },
                        ),
                        Text('Nam',style: TextStyle(
                          fontSize: 16,
                          height: 1,
                          color: KBorderColor
                        ),)
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Radio(
                          value: 2,
                          groupValue: _value,
                          activeColor: Colors.blue,
                          onChanged: (value){
                            setState(() {
                              _value = value.hashCode;
                            });
                          },
                        ),
                        Text('Nữ',style: TextStyle(
                            fontSize: 16,
                            height: 1,
                            color: KBorderColor
                        ),)
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Container(
                height: 56,
                margin: EdgeInsets.only(left: 16, right: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  color: Colors.white,
                  border: Border.all(
                    width: 1, color: KBorderColor.withOpacity(0.1),
                  )
                ),
                child: TextField(
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.keyboard_arrow_down,
                    color:KBorderColor.withOpacity(0.6)),
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 16,right: 16,top: 8),
                    labelText: 'Loại giấy tờ',
                    labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: KBorderColor.withOpacity(0.4),
                        height: 1.2
                    ),
                  ),
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: KBorderColor,
                      height: 1.2
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                height: 56,
                margin: EdgeInsets.only(left: 16, right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white,
                    border: Border.all(
                      width: 1, color: KBorderColor.withOpacity(0.1),
                    )
                ),
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 16,right: 16,top: 8),
                    labelText: 'Mã số',
                    labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: KBorderColor.withOpacity(0.4),
                        height: 1.2
                    ),
                  ),
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: KBorderColor,
                      height: 1.2
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                height: 56,
                margin: EdgeInsets.only(left: 16, right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white,
                    border: Border.all(
                      width: 1, color: KBorderColor.withOpacity(0.1),
                    )
                ),
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 16,right: 16,top: 8),
                    labelText: 'Họ và tên',
                    labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: KBorderColor.withOpacity(0.4),
                        height: 1.2
                    ),
                  ),
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: KBorderColor,
                      height: 1.2
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                height: 56,
                margin: EdgeInsets.only(left: 16, right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white,
                    border: Border.all(
                      width: 1, color: KBorderColor.withOpacity(0.1),
                    )
                ),
                child: TextField(
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.calendar_today),
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 16,right: 16,top: 8),
                    labelText: 'Ngày/Tháng/Năm sinh',
                    labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: KBorderColor.withOpacity(0.4),
                        height: 1.2
                    ),
                  ),
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: KBorderColor,
                      height: 1.2
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                height: 56,
                margin: EdgeInsets.only(left: 16, right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white,
                    border: Border.all(
                      width: 1, color: KBorderColor.withOpacity(0.1),
                    )
                ),
                child: TextField(
                  decoration: InputDecoration(
                    suffixIcon: Icon(Icons.calendar_today),
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 16,right: 16,top: 8),
                    labelText: 'Ngày cấp',
                    labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: KBorderColor.withOpacity(0.4),
                        height: 1.2
                    ),
                  ),
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: KBorderColor,
                      height: 1.2
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                height: 56,
                margin: EdgeInsets.only(left: 16, right: 16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Colors.white,
                    border: Border.all(
                      width: 1, color: KBorderColor.withOpacity(0.1),
                    )
                ),
                child: TextField(
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 16,right: 16,top: 8),
                    labelText: 'Nơi cấp',
                    labelStyle: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: KBorderColor.withOpacity(0.4),
                        height: 1.2
                    ),
                  ),
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: KBorderColor,
                      height: 1.2
                  ),
                ),
              ),
              SizedBox(
                height: 78
              ),
              Container(
                height: 110,
                decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(width: 1, color:KBorderColor.withOpacity(0.2))
                    ),
                  color: Colors.white
                ),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          height: 16,
                          width: 16,
                          margin: EdgeInsets.only(left: 16,top: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(2),
                            color: Colors.white,
                            border: Border.all(width: 1,color: KBorderColor)
                          ),
                          child: Checkbox(
                            checkColor: Colors.white,
                            value: _isChecked,
                            onChanged: (bool? value) {
                              setState(() {
                                _isChecked = value!;
                              });
                            },
                          ),

                        ),
                        Container(
                          height: 40,
                          color:Colors.white,
                          margin: EdgeInsets.only(top: 8,bottom:10,left: 46, right: 16),
                          child: Text(
                            'Tôi xác nhận thông tin trên hoàn toàn là thông tin cá nhân của tôi',
                            style: TextStyle(
                                fontWeight: FontWeight.normal,
                                fontSize: 14,
                                color: KBorderColor.withOpacity(0.6),
                                height: 1.2
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 16, right: 16),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            blurRadius: 1,
                            spreadRadius: 2,
                            offset: Offset(0,4),
                          )
                        ],
                      ),
                      child: ButtonTheme(
                        minWidth: 362,
                        height: 44,
                        child: FlatButton(
                          onPressed:(){},
                          color: kkBlueColor,
                          child: Text("Tiếp tục", style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: Colors.white
                          ),),
                        ),
                      ),
                    ),
                  ],
                ),
              )

            ],
          ),
        ),
      ),
    );

  }
}
