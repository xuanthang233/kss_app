import 'package:flutter/material.dart';

const kBlueLightColor = Color(0xFF0F52BA);
const kPrimaryLightColor = Color(0xFFF1E6FF);
const kOrangeColor= Color(0xFFEF4D23);
const kGreyColor= Color(0xFFA7A8AA);
const kkBlueColor= Color(0xFF283991);
const KBorderColor = Color(0xFF333333);

