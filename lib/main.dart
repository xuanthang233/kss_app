import 'package:flutter/material.dart';
import 'package:kss_login/Components/change_password/change_new_password.dart';
import 'package:kss_login/Components/change_password/change_password.dart';
import 'package:kss_login/Components/change_password/change_password_OTP.dart';
import 'package:kss_login/Components/change_password/change_password_phoneNumber.dart';
import 'package:kss_login/Components/forgot_password/forgot_password_OTP.dart';
import 'package:kss_login/Components/forgot_password/forgot_password_phoneNumber.dart';
import 'Components/forgot_password/forgot_password.dart';
import 'Screens/login_screen.dart';
import 'Screens/Login_screen_enable_btn.dart';
import 'Screens/Login_screen_with_shortcut.dart';
import 'Screens/Login_with_shortcut_fingerprint.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'KSS Login',
      debugShowCheckedModeBanner: false,
      home: FogotPassword(),
    );
  }
}

